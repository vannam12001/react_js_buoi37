import { combineReducers } from "redux";
import { StudentReducer } from "./studentReducer";

export const rootReducer = combineReducers({ StudentReducer });
